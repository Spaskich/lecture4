#include <iostream>
#include <cmath>
using namespace std;

int main() {
    int a, b, c;
    cout << "Enter a: ";
    cin >> a;
    cout << "Enter b: ";
    cin >> b;
    cout << "Enter c: ";
    cin >> c;
    double d = pow(b,2) - 4*a*c;
    cout << d << endl;

    if (d < 0){
        cout << "No solutions" << endl;
    }
    else if (d == 0){
        cout << "x = " << -b / (2*a) << endl;
    }
    else{
        cout << "Solutions: " << endl;
        cout << "x1 = " << (-b + sqrt(d)) / (2*a) << endl;
        cout << "x2 = " << (-b - sqrt(d)) / (2*a) << endl;
    }

    return 0;
}
