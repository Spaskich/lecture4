#include <iostream>
using namespace std;

int main() {
    float n1, n2, n3;
    cout << "Enter a number: ";
    cin >> n1;
    cout << "Enter a number: ";
    cin >> n2;
    cout << "Enter a number: ";
    cin >> n3;

    if (n1 > n2 && n1 > n3){
        cout << "The biggest number is " << n1;
    }
    else if (n2 > n1 && n2 > n3){
        cout << "The biggest number is " << n2;
    }
    else if (n3 > n1 && n3 > n2){
        cout << "The biggest number is " << n3;
    }
    else{
        cout << "There's no biggest number";
    }

    return 0;
}
